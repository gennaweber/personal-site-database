CREATE DATABASE  IF NOT EXISTS `personal` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `personal`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: personal
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contactentry`
--

DROP TABLE IF EXISTS `contactentry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contactentry` (
  `entryID` int unsigned NOT NULL AUTO_INCREMENT,
  `entryName` varchar(255) NOT NULL DEFAULT 'anonymous',
  `entryEmail` varchar(225) NOT NULL DEFAULT '',
  `entryContent` text NOT NULL,
  `entrySubmitTime` datetime DEFAULT '1000-01-01 00:00:00',
  PRIMARY KEY (`entryID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contactentry`
--

LOCK TABLES `contactentry` WRITE;
/*!40000 ALTER TABLE `contactentry` DISABLE KEYS */;
INSERT INTO `contactentry` VALUES (1,'John','mrjohn@gmail.com','Test content','2021-08-19 18:41:55');
/*!40000 ALTER TABLE `contactentry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portfolio`
--

DROP TABLE IF EXISTS `portfolio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `portfolio` (
  `portfolioID` int unsigned NOT NULL AUTO_INCREMENT,
  `portfolioDescription` text,
  `userRef` int unsigned DEFAULT NULL,
  PRIMARY KEY (`portfolioID`),
  KEY `userRef` (`userRef`),
  CONSTRAINT `portfolio_ibfk_1` FOREIGN KEY (`userRef`) REFERENCES `user` (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portfolio`
--

LOCK TABLES `portfolio` WRITE;
/*!40000 ALTER TABLE `portfolio` DISABLE KEYS */;
INSERT INTO `portfolio` VALUES (1,'Click the images to learn more about each site I\'ve created or helped create. This particular website was built first using vanilla Javascript and later refactored to use React.',1);
/*!40000 ALTER TABLE `portfolio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project` (
  `projectID` int unsigned NOT NULL AUTO_INCREMENT,
  `projectName` varchar(255) NOT NULL DEFAULT '',
  `projectSubtitle` varchar(1024) DEFAULT NULL,
  `projectLink` varchar(1024) DEFAULT NULL,
  `projectDescription` text,
  `projectImagePath` varchar(1024) DEFAULT NULL,
  `projectAltText` varchar(1024) DEFAULT NULL,
  `categoryRef` int unsigned DEFAULT NULL,
  `portfolioRef` int unsigned DEFAULT NULL,
  `userRef` int unsigned DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `displayOrder` int unsigned NOT NULL DEFAULT '100',
  PRIMARY KEY (`projectID`),
  KEY `categoryRef` (`categoryRef`),
  KEY `portfolioRef` (`portfolioRef`),
  KEY `userRef` (`userRef`),
  CONSTRAINT `project_ibfk_1` FOREIGN KEY (`categoryRef`) REFERENCES `projectcategory` (`projectCatID`),
  CONSTRAINT `project_ibfk_2` FOREIGN KEY (`portfolioRef`) REFERENCES `portfolio` (`portfolioID`),
  CONSTRAINT `project_ibfk_3` FOREIGN KEY (`userRef`) REFERENCES `user` (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES (1,'Email Collector','A Simple Subscription Website','https://gennasemailcollector.herokuapp.com/','This is a simple website I designed using Bootstrap. The backend is configured using Node.js to communicate with the Mailchimp API to manage an email subscription list.','./images/email.jpg','Simple subscription form for a newletter',2,1,1,1,1),(2,'Todo List','A Simple and Clean Web App','https://gennastodolist.herokuapp.com','This little web app has a backend created using Node.js and MongoDB to store the data.','./images/todo.jpg','Purple todo list with date on the top',2,1,1,1,2),(3,'Low German Lesson Website','A Resource for Language Learners','https://plautdietsch-lessons.onrender.com/app/home','This website was a labour of love for the language of my heritage. Built with React, it uses Gatsby to pull data from Wordpress with GraphQL to programmatically create static web pages. The site is protected using simple user athentication and conditional client-side rendering. To top it off, the site is integrated with Algolia for a smooth search experience.','./images/lessons.jpg','Simple, mainly text website with green header',3,1,1,1,3),(4,'Nature Gallery','A Beautiful Photosharing Website','http://www.duchmorri.com/Groupproject/index.html','This is my first group project created to get more practice with Git, css, and Vanilla Javascript. Everyone in the team worked together to create a website that looks good and functions as intended.','./images/gallery.jpg','Gallery website with a photograph of a leaf in the background overlaid with a welcome message',1,1,1,1,4),(5,'dsafdaf','asdfasdf','asfdfasdf','asdfasdf','','asdfasdfasd',1,1,1,0,100),(6,'New','dkfjas','fd;asdjf','dfklasjkldf','','flaksjdfl',1,1,1,0,100);
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projectcategory`
--

DROP TABLE IF EXISTS `projectcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `projectcategory` (
  `projectCatID` int unsigned NOT NULL AUTO_INCREMENT,
  `projectCatName` varchar(255) NOT NULL DEFAULT '',
  `resumeRef` int unsigned DEFAULT NULL,
  PRIMARY KEY (`projectCatID`),
  KEY `resumeRef` (`resumeRef`),
  CONSTRAINT `projectcategory_ibfk_1` FOREIGN KEY (`resumeRef`) REFERENCES `resume` (`resumeID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projectcategory`
--

LOCK TABLES `projectcategory` WRITE;
/*!40000 ALTER TABLE `projectcategory` DISABLE KEYS */;
INSERT INTO `projectcategory` VALUES (1,'Javascript',1),(2,'Node.js',1),(3,'React',1);
/*!40000 ALTER TABLE `projectcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resume`
--

DROP TABLE IF EXISTS `resume`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `resume` (
  `resumeID` int unsigned NOT NULL AUTO_INCREMENT,
  `resumeDescription` text,
  `userRef` int unsigned DEFAULT NULL,
  PRIMARY KEY (`resumeID`),
  KEY `userRef` (`userRef`),
  CONSTRAINT `resume_ibfk_1` FOREIGN KEY (`userRef`) REFERENCES `user` (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resume`
--

LOCK TABLES `resume` WRITE;
/*!40000 ALTER TABLE `resume` DISABLE KEYS */;
INSERT INTO `resume` VALUES (1,'Hi, I\'m Genna Weber. I\'m a 23-year-old web developer in Southwestern Ontario. I\'m currently a student at York University School of Continuing Studies in the Full Stack Web Development certificate program set to graduate Dec 2021. \n I\'ve been working remotely for the past 4 years working as a Medical Language Specialist. I\'m passionate about coding and eager to keep learning.',1);
/*!40000 ALTER TABLE `resume` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skill`
--

DROP TABLE IF EXISTS `skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `skill` (
  `skillID` int unsigned NOT NULL AUTO_INCREMENT,
  `skillName` varchar(255) NOT NULL DEFAULT '',
  `skillRating` tinyint unsigned NOT NULL DEFAULT '0',
  `categoryRef` int unsigned DEFAULT NULL,
  `resumeRef` int unsigned DEFAULT NULL,
  `userRef` int unsigned DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`skillID`),
  KEY `categoryRef` (`categoryRef`),
  KEY `resumeRef` (`resumeRef`),
  KEY `userRef` (`userRef`),
  CONSTRAINT `skill_ibfk_1` FOREIGN KEY (`categoryRef`) REFERENCES `skillcategory` (`skillCatID`),
  CONSTRAINT `skill_ibfk_2` FOREIGN KEY (`resumeRef`) REFERENCES `resume` (`resumeID`),
  CONSTRAINT `skill_ibfk_3` FOREIGN KEY (`userRef`) REFERENCES `user` (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skill`
--

LOCK TABLES `skill` WRITE;
/*!40000 ALTER TABLE `skill` DISABLE KEYS */;
INSERT INTO `skill` VALUES (1,'HTML5',4,1,1,1,1),(2,'CSS',4,1,1,1,1),(3,'Bootstrap',3,1,1,1,1),(4,'Javascript',4,1,1,1,1),(5,'React',4,1,1,1,1),(6,'Gatsby',3,1,1,1,1),(7,'Node.js',4,2,1,1,1),(8,'Express.js',3,2,1,1,1),(9,'PHP',2,2,1,1,1),(10,'MySQL',2,2,1,1,1),(11,'MongoDB',1,2,1,1,1),(12,'Heroku CLI',1,2,1,1,1),(13,'Git',4,3,1,1,1),(14,'Traditional Art',4,3,1,1,1),(15,'Photoshop',3,3,1,1,1),(16,'Procreate',3,3,1,1,1);
/*!40000 ALTER TABLE `skill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skillcategory`
--

DROP TABLE IF EXISTS `skillcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `skillcategory` (
  `skillCatID` int unsigned NOT NULL AUTO_INCREMENT,
  `skillCatName` varchar(255) NOT NULL DEFAULT '',
  `skillCatColor` varchar(255) NOT NULL DEFAULT '',
  `skillCatIcon` varchar(1024) NOT NULL DEFAULT '',
  `resumeRef` int unsigned DEFAULT NULL,
  PRIMARY KEY (`skillCatID`),
  KEY `resumeRef` (`resumeRef`),
  CONSTRAINT `skillcategory_ibfk_1` FOREIGN KEY (`resumeRef`) REFERENCES `resume` (`resumeID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skillcategory`
--

LOCK TABLES `skillcategory` WRITE;
/*!40000 ALTER TABLE `skillcategory` DISABLE KEYS */;
INSERT INTO `skillcategory` VALUES (1,'Client Side','purple','<i class=\'fas fa-laptop-code fa-2x\'></i>',1),(2,'Server Side','blue','<i class=\'fas fa-cogs fa-2x\'></i>',1),(3,'Design & More','grey','<i class=\'far fa-object-ungroup fa-2x\'></i>',1);
/*!40000 ALTER TABLE `skillcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `userID` int unsigned NOT NULL AUTO_INCREMENT,
  `userEmail` varchar(255) NOT NULL DEFAULT '',
  `userName` varchar(255) DEFAULT '',
  `userPassword` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'gennamweber@gmail.com','Genna Weber','$2b$10$7/R5nxUIr.4ss7ol3QJQ..I0yf/UlFRrU341ZN4nzy/6eTF9WfEQm');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-20 16:31:51
